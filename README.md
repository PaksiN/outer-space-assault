# Outer Space Assault

A játék a klasszikus Space Invaders Top-Down shooter 3D újragondolása lesz.
A csavar benne, hogy 2 síkon tudunk majd mozogni, és ezt kihasználva kell manőverezni az egyre nehezedő pályán.
Elsődlegesen PC-re készül, de a touchscreen vezérlést is szeretném megoldani.

## Tervezett funkciók
   - Folyamatosan generált pálya
   - Pontok szerzése
   - Toplista

A megvalósításhoz Unity 2020.2.5f1 verziót fogok használni.
Unity Package-ek közül az újabb Input Managert és a Cinemachine package-eket hívom segítségül.

## Készítette: Paksi Norbert