using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;

    private float movementX;
    private float movementY;
    [SerializeField] private float forwardSpeed = 20f, strafeSpeed = 15f;
    [SerializeField] private float yMin, yMax, xMin, xMax;
    [SerializeField] private GameObject projectile, gameOver;
    [SerializeField] private float launchVelocity = 700f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        ProccessMovement();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Player Triggered Something " + other.tag);
        Time.timeScale = 0;
        gameOver.SetActive(true);
    }


    public void OnMovement(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        
        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    private void ProccessMovement()
    {
        float xOffset = movementX * strafeSpeed * Time.deltaTime;
        float yOffset = movementY * strafeSpeed * Time.deltaTime;

        float rawNewXPos = transform.localPosition.x + xOffset;
        float rawNewYPos = transform.localPosition.y + yOffset;

        float clampedX = Mathf.Clamp(rawNewXPos, -xMin, xMax);
        float clampedY = Mathf.Clamp(rawNewYPos, -yMin, yMax);

        transform.localPosition = new Vector3(clampedX, clampedY, transform.localPosition.z + (forwardSpeed * Time.deltaTime) );
    }

    public void OnShoot()
    {
        Transform launcher = this.GetComponentInChildren<Transform>();
       
        GameObject proj = Instantiate(projectile, launcher.position, launcher.rotation);
    }
}
