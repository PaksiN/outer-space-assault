using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject[] spawnabbleObjects;

    void Start()
    {
        InvokeRepeating("Spawn", 1f, 5f);
    }
    private void Spawn()
    {
        int randomNumber = Random.Range(0, spawnabbleObjects.Length);
        Instantiate(spawnabbleObjects[randomNumber], new Vector3( 0, 0, transform.position.z), transform.rotation);
    }
}
