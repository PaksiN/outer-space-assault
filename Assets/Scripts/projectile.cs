using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectile : MonoBehaviour
{
    [SerializeField] private float projSpeed = 20f;

    void Update()
    {
        transform.Translate(new Vector3(0, 0, projSpeed));
        Object.Destroy(this.gameObject, 3f);
    }

    public void OnTriggerEnter(Collider collision)
    {
        Debug.Log("Collision with "+ collision.tag);
        if (collision.tag == "Enemy")
        {
            Destroy(collision.gameObject);
            GameManager.score += 1;
        }
        Destroy(this.gameObject);
    }
}
